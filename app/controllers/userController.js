//import thư viện userModel
const { default: mongoose } = require('mongoose');
const userModel = require('../models/userModel');

//create new user:
const createUser = async (req, res) => {
    //B1: thu thập dữ liệu:
    const { fullName, email, address, phone } = req.body;
    //B2: validate dữ liệu:
    if (!fullName) {
        return res.status(400).json({
            status: `Bad request`,
            message: `fullName is required!`
        })
    }
    if (!email) {
        return res.status(400).json({
            status: `Bad request`,
            message: `email is required!`
        })
    }
    if (!address) {
        return res.status(400).json({
            status: `Bad request`,
            message: `address is required!`
        })
    }
    if (!phone) {
        return res.status(400).json({
            status: `Bad request`,
            message: `phone is required!`
        })
    }
    //B3: thực thi model
    let createUserData = {
        _id: new mongoose.Types.ObjectId(),
        fullName: fullName,
        email: email,
        address: address,
        phone: phone
    }
    try {
        const createdUser = await userModel.create(createUserData);
        return res.status(201).json({
            status: `Create new user successfully`,
            data: createdUser
        })
    }
    catch (error) {
        return res.status(500).json({
            status: `Internal Server Error`,
            message: error.message
        })
    }
}

//Get All User:
const getAllUsers = async (req, res) => {
    try {
        const userList = await userModel.find()
        if (userList || userList.length > 0) {
            return res.status(200).json({
                status: `Get all users successfully!`,
                data: userList
            })
        } else {
            return res.status(404).json({
                status: `Not found any users`,
                data
            })
        }

    }
    catch (error) {
        return res.status(500).json({
            status: `Internal Server Error`,
            message: error.message
        })
    }
}

//Get user by Id:
const getUserById = async (req, res) => {
    //B1: thu thap du lieu
    const userId = req.params.userId;
    //B2: validate du lieu:
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `phone is required!`
        })
    }
    try {
        const userFoundById = await userModel.findById(userId);
        if (userFoundById) {
            return res.status(200).json({
                status: `Get user by id successfully`,
                data: userFoundById
            })
        } else {
            return res.status(404).json({
                status: `Do not found any users`,
                data
            })
        }
    }
    catch (error) {
        return res.status(500).json({
            status: `Internal Server Error`,
            message: error.message
        })
    }
}

//Update user by id
const updateUserById = (req, res) => {
    const userId = req.params.userId;
    const { fullName, email, address, phone } = req.body;
    //B2: kiểm tra dữ liệu:
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `userId in invalid!`
        })
    }

    if (!fullName) {
        return res.status(400).json({
            status: `Bad request`,
            message: `fullName is required!`
        })
    }
    if (!email) {
        return res.status(400).json({
            status: `Bad request`,
            message: `email is required!`
        })
    }
    if (!address) {
        return res.status(400).json({
            status: `Bad request`,
            message: `address is required!`
        })
    }
    if (!phone) {
        return res.status(400).json({
            status: `Bad request`,
            message: `phone is required!`
        })
    }
    //B3: thực thi model:

    let updateUserData = {
        fullName,
        email,
        address,
        phone
    }
    userModel.findByIdAndUpdate(userId, updateUserData)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: "Update user successfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: `Not found any users`
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}
//Delete by user by id:
const deleteUserById = async(req, res) => {
    //B1: thu thập dữ liệu
    const userId = req.params.userId;
    //B2: kiểm tra dữ liệu:
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `User id is invalid`
        })
    }
    try {
       const deletedUser = await userModel.findByIdAndDelete(userId);
       if(deletedUser) {
        return res.status(204).json({
            status: `Delete user by id successfully`,
            data: deletedUser
        })
       }else{
        return res.status(404).json({
            status: `Not found any users`,
        })
       }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
    
}
module.exports = {
    createUser,
    getAllUsers,
    getUserById,
    updateUserById,
    deleteUserById
}