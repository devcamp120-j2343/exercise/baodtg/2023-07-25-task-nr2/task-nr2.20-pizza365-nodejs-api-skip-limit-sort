//import thư viện userModel
const { default: mongoose } = require('mongoose');
const userModel = require('../models/userModel');

//Get Limit All User:
const getLimitAllUsers = async (req, res) => {
    const limit = req.query.limit
    try {
        const userList = await userModel.find().limit(limit);
        if (userList || userList.length > 0) {
            return res.status(200).json({
                status: `Get all users successfully!`,
                data: userList
            })
        } else {
            return res.status(404).json({
                status: `Not found any users`,
                data
            })
        }

    }
    catch (error) {
        return res.status(500).json({
            status: `Internal Server Error`,
            message: error.message
        })
    }
}

//Get Skip All User:
const getSkipAllUsers = async (req, res) => {
    const skip = req.query.skip
    try {
        const userList = await userModel.find().skip(skip);
        if (userList || userList.length > 0) {
            return res.status(200).json({
                status: `Get all users successfully!`,
                data: userList
            })
        } else {
            return res.status(404).json({
                status: `Not found any users`,
                data
            })
        }

    }
    catch (error) {
        return res.status(500).json({
            status: `Internal Server Error`,
            message: error.message
        })
    }
}
//Get Sort All User:
const getSortAllUsers = async (req, res) => {
    try {
        const userList = await userModel.find().sort({fullName: "1"});
        if (userList || userList.length > 0) {
            return res.status(200).json({
                status: `Get all users successfully!`,
                data: userList
            })
        } else {
            return res.status(404).json({
                status: `Not found any users`,
                data
            })
        }

    }
    catch (error) {
        return res.status(500).json({
            status: `Internal Server Error`,
            message: error.message
        })
    }
}

//Get Skip Limit All User:
const getSkipLimitAllUsers = async (req, res) => {
    const skip = req.query.skip;
    const limit = req.query.limit;
    try {
        const userList = await userModel.find().skip(skip).limit(limit);
        if (userList || userList.length > 0) {
            return res.status(200).json({
                status: `Get all users successfully!`,
                data: userList
            })
        } else {
            return res.status(404).json({
                status: `Not found any users`,
                data
            })
        }

    }
    catch (error) {
        return res.status(500).json({
            status: `Internal Server Error`,
            message: error.message
        })
    }
}

//Get Sort Skip Limit All User:
const getSortSkipLimitAllUsers = async (req, res) => {
    const skip = req.query.skip;
    const limit = req.query.limit;
    try {
        const userList = await userModel.find().sort({fullName: "1"}).limit(limit).skip(skip);
        if (userList || userList.length > 0) {
            return res.status(200).json({
                status: `Get all users successfully!`,
                data: userList
            })
        } else {
            return res.status(404).json({
                status: `Not found any users`,
                data
            })
        }

    }
    catch (error) {
        return res.status(500).json({
            status: `Internal Server Error`,
            message: error.message
        })
    }
}
module.exports = {getLimitAllUsers, getSkipAllUsers, getSortAllUsers, getSkipLimitAllUsers, getSortSkipLimitAllUsers}